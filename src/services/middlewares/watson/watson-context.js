import mongoose from '../../mongoose'
import { Schema } from 'mongoose'
import { Transaction } from '../../../api/transaction'
var MongoClient = require('mongodb').MongoClient;
const utils = require("../../../utils.js");
var _ = require('lodash');

function createContextFromTransaction(req, res, foundTransaction, next){
  console.log("> createContextFromTransaction");
  req.session.activeTransaction = foundTransaction;
  if(req.session.setContext){
    console.log("> SET Context");
    req.session.watsonContext = {
      "sender_month": foundTransaction.request_month,
      "sender_value": foundTransaction.request_value,
      "sender_option": foundTransaction.request_action,
      "sender_quantity": foundTransaction.request_quantity
    }
  }else {
    console.log("> UPDATE Context");
    req.session.watsonContext.sender_month = foundTransaction.request_month;
    req.session.watsonContext.sender_value = foundTransaction.request_value;
    req.session.watsonContext.sender_option = foundTransaction.request_action;
    req.session.watsonContext.sender_quantity = foundTransaction.request_quantity;
    if(foundTransaction.trade_action == "bid"){
      req.session.watsonContext.sender_bid = foundTransaction.trade_value;
    }else {
      req.session.watsonContext.sender_offer = foundTransaction.trade_value;
    }
    console.log(req.session.watsonContext);
  }

  next();
}

const WatsonContextMiddleware = (options) => {

  const watsonContextController = (req, res, next) => {
    console.log("> Middleware - WatsonContextMiddleware: " + req.body.senderId);
    var sessionData = req.session;
    var watsonUpdate = req.session.watsonUpdate;
    req.session.setContext = false;

    if(_.indexOf(utils.MARKET_MAKERS, parseInt(req.body.senderId)) >= 0){
      console.log("> Set user type: " + utils.USER_TYPE_MARKET_MAKER);
      req.session.userType = utils.USER_TYPE_MARKET_MAKER;
    }

    // If Market Maker
    if(_.indexOf(utils.MARKET_MAKERS, parseInt(req.body.senderId)) >= 0){
      if(!req.session.watsonContext){
        req.session.setContext = true;
        console.log("NO CONTEXT");
      }else {
        req.session.setContext = false;
        console.log("MARKET MAKER HAS CONTEXT");
      }

      // Find transaction for this MarketMaker to set CONTEXT
      Transaction.find({
        'marketMakerId': req.body.senderId
      })
      .then((transactions) => {
          var counter = 0;
          var transactionFound = null;
          MongoClient.connect("mongodb://localhost/aix-bot-api", function(err, db) {
            if(err) { return console.dir(err); }

            var collection = db.collection('sessions');
            collection.find().toArray(function(err, sessions) {
                for(var i=0; i < transactions.length; i++){
                  sessions.forEach(function(session){
                    if(transactions[i].traderSessionId == session._id){
                      transactionFound = transactions[i]
                    }
                  });
                }
                if(transactionFound){
                  createContextFromTransaction(req, res, transactionFound, next);
                }else {
                  req.session.setContext = false;
                  next();
                }
            });
          });
      })
    }else {
      next();
    }
  }

  return watsonContextController;
}

export default new WatsonContextMiddleware();
