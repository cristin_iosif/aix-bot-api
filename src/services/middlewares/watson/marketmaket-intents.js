import { Transaction } from '../../../api/transaction'
const utils = require("../../../utils.js");

const MarketMakerIntentsMiddleware = (options) => {

  const marketMakerIntentsController = (req, res, next) => {
    console.log("> Middleware - MarketMakerIntentsMiddleware");
    var sessionData = req.session;
    var watsonUpdate = req.session.watsonUpdate;

    if(watsonUpdate.context.intent_found && req.session.userType == utils.USER_TYPE_MARKET_MAKER){
      var marketMakerIntent = {};
      switch (watsonUpdate.context.intent_name) {
        case "two_way_quote":
          marketMakerIntent.intent = watsonUpdate.context.intent_name;

          var quote_bid = watsonUpdate.context.quote_bid;
          var quote_offer = watsonUpdate.context.quote_offer;

          Transaction.findOne({
            'traderSessionId': sessionData.activeTransaction.traderSessionId,
            'marketMakerId': req.body.senderId
          })
          .then((transaction) => {
            transaction.quote_bid = quote_bid;
            transaction.quote_offer = quote_offer;
            transaction.save((err, transaction) => {
              sessionData.activeTransaction = transaction;
              Transaction.find(
                {
                  'traderSessionId': sessionData.activeTransaction.traderSessionId,
              })
              .then((traderTransactions) => {
                console.log("traderTransactions" + traderTransactions.length);
                var sendBidToTrader = true;
                var minSpreadValue = Number.MAX_SAFE_INTEGER;
                var minSpreadBid;
                var minSpreadOffer;
                for(var i=0; i < traderTransactions.length; i++){
                  if(!traderTransactions[i].quote_bid || !traderTransactions[i].quote_offer){
                    sendBidToTrader = false;
                  }else {
                    if((traderTransactions[i].quote_offer - traderTransactions[i].quote_bid) < minSpreadValue){
                      minSpreadValue = traderTransactions[i].quote_offer - traderTransactions[i].quote_bid;
                      minSpreadBid = traderTransactions[i].quote_bid;
                      minSpreadOffer = traderTransactions[i].quote_offer;
                    }
                  }
                }
                console.log("> Notify Trader of bid: " + sendBidToTrader);
                if(sendBidToTrader){
                  marketMakerIntent.traderTelegramId = sessionData.activeTransaction.traderId;
                  marketMakerIntent.traderMessage = utils.CURRENCY_SYMBOL + minSpreadBid + '/' + utils.CURRENCY_SYMBOL + minSpreadOffer + ' for ' + transaction.request_quantity +' barrels of ' + transaction.request_month + ' ' + transaction.request_value + '$ strike ' + transaction.request_action;
                }

                sessionData.marketMakerIntent = marketMakerIntent;
                next();
              })
              // determin if we notify Trader
            });
          })
          break;

        case "accept_offer":
          var activeTransaction = sessionData.activeTransaction;
          marketMakerIntent.intent = watsonUpdate.context.intent_name;
          Transaction.findOne({
            'traderSessionId': sessionData.activeTransaction.traderSessionId,
            'marketMakerId': req.body.senderId
          })
          .then((transaction) => {
            marketMakerIntent.traderTelegramId = sessionData.activeTransaction.traderId;
            marketMakerIntent.traderMessage = 'That\'s confirmed you buy '+ transaction.request_quantity+' barrels of ' + transaction.request_month + ' 2018 $'+ transaction.request_value+' strike WTI Crude calls at $'+ transaction.trade_value +' per barrel, cleared through the CME.  Email confirmations have been sent. Thank you for the business.'
            sessionData.marketMakerIntent = marketMakerIntent;
            next();
          });

          break;

          default:
            next();
      }
    }else {
      next();
    }
  }

  return marketMakerIntentsController;
}

export default new MarketMakerIntentsMiddleware();
