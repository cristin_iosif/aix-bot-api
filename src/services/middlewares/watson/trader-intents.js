import { Transaction } from '../../../api/transaction'
const utils = require("../../../utils.js");
var _ = require('lodash');

const TraderIntentsMiddleware = (options) => {

  const traderIntentsController = (req, res, next) => {
    console.log("> Middleware - TraderIntentsMiddleware");
    var sessionData = req.session;
    var watsonUpdate = req.session.watsonUpdate;

    if(watsonUpdate.context.intent_found){
      var traderIntent = {};
      switch (watsonUpdate.context.intent_name) {
        case "rate_request":
          // Values from Trader
          const requestMonth = watsonUpdate.context.request_month;
          const requestValue = watsonUpdate.context.request_value;
          const requestAction = watsonUpdate.context.request_action; // Put or Call
          const requestQuantity = watsonUpdate.context.request_quantity;
          const trade_has_delta = watsonUpdate.context.trade_has_delta;

          traderIntent.intent = watsonUpdate.context.intent_name;
          traderIntent.marketMakersTelegramIds = utils.MARKET_MAKERS;
          traderIntent.marketMakerMessage = `Hello MarketMaker, I’m looking for a two way price in ${requestQuantity} barrels of ${requestMonth} 2018 $${requestValue} strike WTI Crude ${requestAction}, where would you be on that please?`;

          for(var i=0; i < utils.MARKET_MAKERS.length; i++){
            // Save transaction
            const transactionData = {
              'traderSessionId': sessionData.id,
              'traderId': req.body.senderId,
              'marketMakerId': utils.MARKET_MAKERS[i],
              'request_month': requestMonth,
              'request_value': requestValue,
              'request_action': requestAction,
              'request_quantity': requestQuantity,
              'trade_has_delta': trade_has_delta
            }

            Transaction.create(transactionData)
              .then(() => {
                next();
              })
              .catch(next)
          }

          break;

        case "trade_request":
          console.log('trade_request');

          const tradeValue = watsonUpdate.context.trade_value;
          const tradeAction = watsonUpdate.context.trade_action;

          traderIntent.intent = watsonUpdate.context.intent_name;
          traderIntent.marketMakersTelegramIds = [491337682, 499418762];

          Transaction.find({
            'traderSessionId': sessionData.id
          })
          .then((transactions) => {
            traderIntent.marketMakerMessage = [];
            var total = transactions.length;
            var count = 0;
            for(var i=0; i < transactions.length; i++){
              transactions[i].trade_value = tradeValue;
              transactions[i].trade_action = tradeAction;
              transactions[i].save((err, trans) => {
                if(err) {
                  console.error(err);
                }else {
                  traderIntent.marketMakerMessage.push({
                    'marketMakersTelegramId': trans.marketMakerId,
                    'marketMakerMessage': 'Hi MarketMaker, I can show you a $' + trans.trade_value + ' bid in ' + trans.request_quantity + ' barrels of ' + trans.request_month + ' 2018 $' + trans.request_value + ' strike WTI Crude ' + trans.request_action + ' against your two way price of $'+ trans.quote_bid +'/$'+ trans.quote_offer + ', so now the tightest price we have on this is $' + trans.trade_value + '/$'+ trans.quote_offer +', your offer'
                  });
                }
                count++;
                if(count >= total){
                  next();
                }
              });
            }

          });

          break;

          default:
            next();

      }

      sessionData.traderIntent = traderIntent;
    }else {
      // Clear any previous set trader intents
      if(sessionData.traderIntent){
        sessionData.traderIntent = null;
      }
      next();
    }
  }

  return traderIntentsController;
}

export default new TraderIntentsMiddleware();
