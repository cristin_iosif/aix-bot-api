'use strict';

const debug = require('debug')('botmaster:watson-conversation-middleware');
const watsonDeveloperCloud = require('watson-developer-cloud');
const utils = require("../../utils.js");
var _ = require('lodash');

const watsonReciverOptions = {
  settings: {
    username: 'ce2c3502-9292-4b58-8ee6-566f2e85fc61',
		password: 'x6DqA5YfLG2a',
    version: 'v1', // as of this writing (01 Apr 2017), only v1 is available
    version_date: '2017-05-26', // latest version-date as of this writing
  },
  //workspaceId: 'ffeb9902-5d3f-4cff-9558-a6f4320e13a7' // As provided by Watson Conversation
  workspaceId: '9fe3eb90-a86d-405d-aac4-310cce5d41ac'
}

const sendMessageToWatson = (params) => {
  const messageForWatson = {
    context: params.context,
    workspace_id: params.workspaceId,
    input: {
      text: params.text,
    },
  };
  return new Promise((resolve, reject) => {
    params.watson.message(messageForWatson, (err, watsonUpdate) => {
      if (err) {
        return reject(err);
      }
      return resolve(watsonUpdate);
    });
  });
};

const AixWatsonConversationWare = (options) => {
  // if (!options || !options.settings || !options.workspaceId) {
  //   throw new Error('In order to create a watson conversation middleware, ' +
  //   'you need to pass in options that contain settings and workspaceId keys');
  // }
  const watson = watsonDeveloperCloud.conversation(watsonReciverOptions.settings);
  const watsonReceiver = watsonDeveloperCloud.conversation(watsonReciverOptions.settings);

  const watsonConversationWareController = (req, res, next) => {
      console.log("> AixWatsonConversationWare");
      var sessionData = req.session;
      var userText = req.body.message;

      if (!sessionData) {
        return Promise.reject(new Error('Watson conversation ware needs ' +
        'to be used with SessionWare.'));
      }
      if (!userText) {
        debug('Got an update with no text, not sending it to Watson');
        return Promise.resolve();
      }

      var workspaceId = null;
      if(req.session.userType == utils.USER_TYPE_MARKET_MAKER){
        workspaceId = "9fe3eb90-a86d-405d-aac4-310cce5d41ac";
      }else {
        workspaceId = "ffeb9902-5d3f-4cff-9558-a6f4320e13a7";
      }

      var context = sessionData.watsonContext;
      if(sessionData.setContext){
        return sendMessageToWatson({
          context,
          text: "/set_context",
          watson,
          workspaceId:  workspaceId,
        })
        .then((watsonUpdate) => {
          sessionData.watsonUpdate = watsonUpdate;
          sessionData.watsonContext = watsonUpdate.context;
          // also expose the watson-develop-cloud conversation object created
          // under the hood as user might want to use it to do other stuff
          sessionData.watsonConversation = watson;
          sessionData.outputMessage = watsonUpdate.output.text[0];
          next();
        })
      }else {
        return sendMessageToWatson({
          context,
          text: userText,
          watson,
          workspaceId:  workspaceId,
        })
        .then((watsonUpdate) => {
          sessionData.watsonUpdate = watsonUpdate;
          sessionData.watsonContext = watsonUpdate.context;
          // also expose the watson-develop-cloud conversation object created
          // under the hood as user might want to use it to do other stuff
          sessionData.watsonConversation = watson;
          sessionData.outputMessage = watsonUpdate.output.text[0];
          next()
        });
      }
    };

    return watsonConversationWareController;
};

module.exports = AixWatsonConversationWare;
