import mongoose, { Schema } from 'mongoose'

const botMessageSchema = new Schema({
  message: {
    type: String
  },
  senderId: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

botMessageSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      message: this.message,
      senderId: this.senderId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('BotMessage', botMessageSchema)

export const schema = model.schema
export default model
