import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export BotMessage, { schema } from './model'
import watsonContextMiddleware from '../../services/middlewares/watson/watson-context.js'
import WatsonConversationWare from '../../services/middlewares/watson-conversation.js'
import watsonTraderIntentsMiddleware from '../../services/middlewares/watson/trader-intents'
import watsonMarketMakerIntentsMiddleware from '../../services/middlewares/watson/marketmaket-intents'
import { success, notFound } from '../../services/response/'

const router = new Router()
const { message, senderId } = schema.tree

/**
 * @api {post} /bot-messages Create bot message
 * @apiName CreateBotMessage
 * @apiGroup BotMessage
 * @apiParam message Bot message's message.
 * @apiParam senderId Bot message's senderId.
 * @apiSuccess {Object} botMessage Bot message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bot message not found.
 */
 const watsonConversationWare = WatsonConversationWare({});
router.post('/',
  body({ message, senderId }),
  watsonContextMiddleware,
  watsonConversationWare,
  create,
  watsonMarketMakerIntentsMiddleware,
  watsonTraderIntentsMiddleware,
  function(req, res) {
    console.log("response");
    const r = {
      'outputMessage': req.session.outputMessage,
      'traderIntent': req.session.traderIntent,
      'marketMakerIntent': req.session.marketMakerIntent,
      'watsonUpdate': req.session.watsonUpdate
    };
    res.send(r);
  })

/**
 * @api {get} /bot-messages Retrieve bot messages
 * @apiName RetrieveBotMessages
 * @apiGroup BotMessage
 * @apiUse listParams
 * @apiSuccess {Object[]} botMessages List of bot messages.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /bot-messages/:id Retrieve bot message
 * @apiName RetrieveBotMessage
 * @apiGroup BotMessage
 * @apiSuccess {Object} botMessage Bot message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bot message not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /bot-messages/:id Update bot message
 * @apiName UpdateBotMessage
 * @apiGroup BotMessage
 * @apiParam message Bot message's message.
 * @apiParam senderId Bot message's senderId.
 * @apiSuccess {Object} botMessage Bot message's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bot message not found.
 */
router.put('/:id',
  body({ message, senderId }),
  update)

/**
 * @api {delete} /bot-messages/:id Delete bot message
 * @apiName DeleteBotMessage
 * @apiGroup BotMessage
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Bot message not found.
 */
router.delete('/:id',
  destroy)

export default router
