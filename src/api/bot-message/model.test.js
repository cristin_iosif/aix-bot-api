import { BotMessage } from '.'

let botMessage

beforeEach(async () => {
  botMessage = await BotMessage.create({ message: 'test', senderId: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = botMessage.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(botMessage.id)
    expect(view.message).toBe(botMessage.message)
    expect(view.senderId).toBe(botMessage.senderId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = botMessage.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(botMessage.id)
    expect(view.message).toBe(botMessage.message)
    expect(view.senderId).toBe(botMessage.senderId)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
