import { success, notFound } from '../../services/response/'
import { Bug } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Bug.create(body)
    .then((bug) => bug.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Bug.count(query)
    .then(count => Bug.find(query, select, cursor)
      .then((bugs) => ({
        count,
        rows: bugs.map((bug) => bug.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Bug.findById(params.id)
    .then(notFound(res))
    .then((bug) => bug ? bug.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Bug.findById(params.id)
    .then(notFound(res))
    .then((bug) => bug ? Object.assign(bug, body).save() : null)
    .then((bug) => bug ? bug.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Bug.findById(params.id)
    .then(notFound(res))
    .then((bug) => bug ? bug.remove() : null)
    .then(success(res, 204))
    .catch(next)
