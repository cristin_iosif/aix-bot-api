import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Bug, { schema } from './model'

const router = new Router()
const { bug_description, conversation_id, user_telegram_id } = schema.tree

/**
 * @api {post} /bugs Create bug
 * @apiName CreateBug
 * @apiGroup Bug
 * @apiParam bug_description Bug's bug_description.
 * @apiParam conversation_id Bug's conversation_id.
 * @apiParam user_telegram_id Bug's user_telegram_id.
 * @apiSuccess {Object} bug Bug's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bug not found.
 */
router.post('/',
  body({ bug_description, conversation_id, user_telegram_id }),
  create)

/**
 * @api {get} /bugs Retrieve bugs
 * @apiName RetrieveBugs
 * @apiGroup Bug
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of bugs.
 * @apiSuccess {Object[]} rows List of bugs.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /bugs/:id Retrieve bug
 * @apiName RetrieveBug
 * @apiGroup Bug
 * @apiSuccess {Object} bug Bug's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bug not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /bugs/:id Update bug
 * @apiName UpdateBug
 * @apiGroup Bug
 * @apiParam bug_description Bug's bug_description.
 * @apiParam conversation_id Bug's conversation_id.
 * @apiParam user_telegram_id Bug's user_telegram_id.
 * @apiSuccess {Object} bug Bug's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Bug not found.
 */
router.put('/:id',
  body({ bug_description, conversation_id, user_telegram_id }),
  update)

/**
 * @api {delete} /bugs/:id Delete bug
 * @apiName DeleteBug
 * @apiGroup Bug
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Bug not found.
 */
router.delete('/:id',
  destroy)

export default router
