import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Bug } from '.'

const app = () => express(apiRoot, routes)

let bug

beforeEach(async () => {
  bug = await Bug.create({})
})

test('POST /bugs 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.bug_description).toEqual('test')
  expect(body.conversation_id).toEqual('test')
  expect(body.user_telegram_id).toEqual('test')
})

test('GET /bugs 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /bugs/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${bug.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bug.id)
})

test('GET /bugs/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /bugs/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${bug.id}`)
    .send({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(bug.id)
  expect(body.bug_description).toEqual('test')
  expect(body.conversation_id).toEqual('test')
  expect(body.user_telegram_id).toEqual('test')
})

test('PUT /bugs/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ bug_description: 'test', conversation_id: 'test', user_telegram_id: 'test' })
  expect(status).toBe(404)
})

test('DELETE /bugs/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${bug.id}`)
  expect(status).toBe(204)
})

test('DELETE /bugs/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
