import mongoose, { Schema } from 'mongoose'

const userSchema = new Schema({
  telegramId: {
    type: String
  },
  jwtToken: {
    type: String
  },
  userType: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

userSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      telegramId: this.telegramId,
      jwtToken: this.jwtToken,
      userType: this.userType,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('User', userSchema)

export const schema = model.schema
export default model
