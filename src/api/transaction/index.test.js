import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Transaction } from '.'

const app = () => express(apiRoot, routes)

let transaction

beforeEach(async () => {
  transaction = await Transaction.create({})
})

test('POST /transactions 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ traderSessionId: 'test', traderId: 'test', marketMakerId: 'test', status: 'test', request_month: 'test', request_value: 'test', request_quantity: 'test', request_action: 'test', trade_value: 'test', trade_action: 'test', trade_delta_reference: 'test', trade_delta_value: 'test', quote_bid: 'test', quote_offer: 'test', market_bid: 'test', market_delta: 'test', market_volatility: 'test', sender_option: 'test', sender_value: 'test', sender_month: 'test', sender_quantity: 'test', sender_delta_reference: 'test', sender_delta_value: 'test', sender_bid: 'test', sender_offer: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.traderSessionId).toEqual('test')
  expect(body.traderId).toEqual('test')
  expect(body.marketMakerId).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.request_month).toEqual('test')
  expect(body.request_value).toEqual('test')
  expect(body.request_quantity).toEqual('test')
  expect(body.request_action).toEqual('test')
  expect(body.trade_value).toEqual('test')
  expect(body.trade_action).toEqual('test')
  expect(body.trade_delta_reference).toEqual('test')
  expect(body.trade_delta_value).toEqual('test')
  expect(body.quote_bid).toEqual('test')
  expect(body.quote_offer).toEqual('test')
  expect(body.market_bid).toEqual('test')
  expect(body.market_delta).toEqual('test')
  expect(body.market_volatility).toEqual('test')
  expect(body.sender_option).toEqual('test')
  expect(body.sender_value).toEqual('test')
  expect(body.sender_month).toEqual('test')
  expect(body.sender_quantity).toEqual('test')
  expect(body.sender_delta_reference).toEqual('test')
  expect(body.sender_delta_value).toEqual('test')
  expect(body.sender_bid).toEqual('test')
  expect(body.sender_offer).toEqual('test')
})

test('GET /transactions 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /transactions/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${transaction.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(transaction.id)
})

test('GET /transactions/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /transactions/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${transaction.id}`)
    .send({ traderSessionId: 'test', traderId: 'test', marketMakerId: 'test', status: 'test', request_month: 'test', request_value: 'test', request_quantity: 'test', request_action: 'test', trade_value: 'test', trade_action: 'test', trade_delta_reference: 'test', trade_delta_value: 'test', quote_bid: 'test', quote_offer: 'test', market_bid: 'test', market_delta: 'test', market_volatility: 'test', sender_option: 'test', sender_value: 'test', sender_month: 'test', sender_quantity: 'test', sender_delta_reference: 'test', sender_delta_value: 'test', sender_bid: 'test', sender_offer: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(transaction.id)
  expect(body.traderSessionId).toEqual('test')
  expect(body.traderId).toEqual('test')
  expect(body.marketMakerId).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.request_month).toEqual('test')
  expect(body.request_value).toEqual('test')
  expect(body.request_quantity).toEqual('test')
  expect(body.request_action).toEqual('test')
  expect(body.trade_value).toEqual('test')
  expect(body.trade_action).toEqual('test')
  expect(body.trade_delta_reference).toEqual('test')
  expect(body.trade_delta_value).toEqual('test')
  expect(body.quote_bid).toEqual('test')
  expect(body.quote_offer).toEqual('test')
  expect(body.market_bid).toEqual('test')
  expect(body.market_delta).toEqual('test')
  expect(body.market_volatility).toEqual('test')
  expect(body.sender_option).toEqual('test')
  expect(body.sender_value).toEqual('test')
  expect(body.sender_month).toEqual('test')
  expect(body.sender_quantity).toEqual('test')
  expect(body.sender_delta_reference).toEqual('test')
  expect(body.sender_delta_value).toEqual('test')
  expect(body.sender_bid).toEqual('test')
  expect(body.sender_offer).toEqual('test')
})

test('PUT /transactions/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ traderSessionId: 'test', traderId: 'test', marketMakerId: 'test', status: 'test', request_month: 'test', request_value: 'test', request_quantity: 'test', request_action: 'test', trade_value: 'test', trade_action: 'test', trade_delta_reference: 'test', trade_delta_value: 'test', quote_bid: 'test', quote_offer: 'test', market_bid: 'test', market_delta: 'test', market_volatility: 'test', sender_option: 'test', sender_value: 'test', sender_month: 'test', sender_quantity: 'test', sender_delta_reference: 'test', sender_delta_value: 'test', sender_bid: 'test', sender_offer: 'test' })
  expect(status).toBe(404)
})

test('DELETE /transactions/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${transaction.id}`)
  expect(status).toBe(204)
})

test('DELETE /transactions/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
