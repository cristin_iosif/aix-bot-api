import mongoose, { Schema } from 'mongoose'

const transactionSchema = new Schema({
  traderSessionId: {
    type: String
  },
  traderId: {
    type: String
  },
  marketMakerId: {
    type: String
  },
  status: {
    type: String
  },
  request_month: {
    type: String
  },
  request_value: {
    type: String
  },
  request_quantity: {
    type: String
  },
  request_action: {
    type: String
  },
  trade_value: {
    type: String
  },
  trade_action: {
    type: String
  },
  trade_has_delta: {
    type: Boolean
  },
  trade_delta_reference: {
    type: String
  },
  trade_delta_value: {
    type: String
  },
  quote_bid: {
    type: String
  },
  quote_offer: {
    type: String
  },
  market_bid: {
    type: String
  },
  market_delta: {
    type: String
  },
  market_volatility: {
    type: String
  },
  sender_option: {
    type: String
  },
  sender_value: {
    type: String
  },
  sender_month: {
    type: String
  },
  sender_quantity: {
    type: String
  },
  sender_delta_reference: {
    type: String
  },
  sender_delta_value: {
    type: String
  },
  sender_bid: {
    type: String
  },
  sender_offer: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

transactionSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      traderSessionId: this.traderSessionId,
      traderId: this.traderId,
      marketMakerId: this.marketMakerId,
      status: this.status,
      request_month: this.request_month,
      request_value: this.request_value,
      request_quantity: this.request_quantity,
      request_action: this.request_action,
      trade_value: this.trade_value,
      trade_action: this.trade_action,
      trade_delta_reference: this.trade_delta_reference,
      trade_delta_value: this.trade_delta_value,
      quote_bid: this.quote_bid,
      quote_offer: this.quote_offer,
      market_bid: this.market_bid,
      market_delta: this.market_delta,
      market_volatility: this.market_volatility,
      sender_option: this.sender_option,
      sender_value: this.sender_value,
      sender_month: this.sender_month,
      sender_quantity: this.sender_quantity,
      sender_delta_reference: this.sender_delta_reference,
      sender_delta_value: this.sender_delta_value,
      sender_bid: this.sender_bid,
      sender_offer: this.sender_offer,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Transaction', transactionSchema)

export const schema = model.schema
export default model
