import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Transaction, { schema } from './model'

const router = new Router()
const { traderSessionId, traderId, marketMakerId, status, request_month, request_value, request_quantity, request_action, trade_value, trade_action, trade_delta_reference, trade_delta_value, quote_bid, quote_offer, market_bid, market_delta, market_volatility, sender_option, sender_value, sender_month, sender_quantity, sender_delta_reference, sender_delta_value, sender_bid, sender_offer } = schema.tree

/**
 * @api {post} /transactions Create transaction
 * @apiName CreateTransaction
 * @apiGroup Transaction
 * @apiParam traderSessionId Transaction's traderSessionId.
 * @apiParam traderId Transaction's traderId.
 * @apiParam marketMakerId Transaction's marketMakerId.
 * @apiParam status Transaction's status.
 * @apiParam request_month Transaction's request_month.
 * @apiParam request_value Transaction's request_value.
 * @apiParam request_quantity Transaction's request_quantity.
 * @apiParam request_action Transaction's request_action.
 * @apiParam trade_value Transaction's trade_value.
 * @apiParam trade_action Transaction's trade_action.
 * @apiParam trade_delta_reference Transaction's trade_delta_reference.
 * @apiParam trade_delta_value Transaction's trade_delta_value.
 * @apiParam quote_bid Transaction's quote_bid.
 * @apiParam quote_offer Transaction's quote_offer.
 * @apiParam market_bid Transaction's market_bid.
 * @apiParam market_delta Transaction's market_delta.
 * @apiParam market_volatility Transaction's market_volatility.
 * @apiParam sender_option Transaction's sender_option.
 * @apiParam sender_value Transaction's sender_value.
 * @apiParam sender_month Transaction's sender_month.
 * @apiParam sender_quantity Transaction's sender_quantity.
 * @apiParam sender_delta_reference Transaction's sender_delta_reference.
 * @apiParam sender_delta_value Transaction's sender_delta_value.
 * @apiParam sender_bid Transaction's sender_bid.
 * @apiParam sender_offer Transaction's sender_offer.
 * @apiSuccess {Object} transaction Transaction's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Transaction not found.
 */
router.post('/',
  body({ traderSessionId, traderId, marketMakerId, status, request_month, request_value, request_quantity, request_action, trade_value, trade_action, trade_delta_reference, trade_delta_value, quote_bid, quote_offer, market_bid, market_delta, market_volatility, sender_option, sender_value, sender_month, sender_quantity, sender_delta_reference, sender_delta_value, sender_bid, sender_offer }),
  create)

/**
 * @api {get} /transactions Retrieve transactions
 * @apiName RetrieveTransactions
 * @apiGroup Transaction
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of transactions.
 * @apiSuccess {Object[]} rows List of transactions.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /transactions/:id Retrieve transaction
 * @apiName RetrieveTransaction
 * @apiGroup Transaction
 * @apiSuccess {Object} transaction Transaction's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Transaction not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /transactions/:id Update transaction
 * @apiName UpdateTransaction
 * @apiGroup Transaction
 * @apiParam traderSessionId Transaction's traderSessionId.
 * @apiParam traderId Transaction's traderId.
 * @apiParam marketMakerId Transaction's marketMakerId.
 * @apiParam status Transaction's status.
 * @apiParam request_month Transaction's request_month.
 * @apiParam request_value Transaction's request_value.
 * @apiParam request_quantity Transaction's request_quantity.
 * @apiParam request_action Transaction's request_action.
 * @apiParam trade_value Transaction's trade_value.
 * @apiParam trade_action Transaction's trade_action.
 * @apiParam trade_delta_reference Transaction's trade_delta_reference.
 * @apiParam trade_delta_value Transaction's trade_delta_value.
 * @apiParam quote_bid Transaction's quote_bid.
 * @apiParam quote_offer Transaction's quote_offer.
 * @apiParam market_bid Transaction's market_bid.
 * @apiParam market_delta Transaction's market_delta.
 * @apiParam market_volatility Transaction's market_volatility.
 * @apiParam sender_option Transaction's sender_option.
 * @apiParam sender_value Transaction's sender_value.
 * @apiParam sender_month Transaction's sender_month.
 * @apiParam sender_quantity Transaction's sender_quantity.
 * @apiParam sender_delta_reference Transaction's sender_delta_reference.
 * @apiParam sender_delta_value Transaction's sender_delta_value.
 * @apiParam sender_bid Transaction's sender_bid.
 * @apiParam sender_offer Transaction's sender_offer.
 * @apiSuccess {Object} transaction Transaction's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Transaction not found.
 */
router.put('/:id',
  body({ traderSessionId, traderId, marketMakerId, status, request_month, request_value, request_quantity, request_action, trade_value, trade_action, trade_delta_reference, trade_delta_value, quote_bid, quote_offer, market_bid, market_delta, market_volatility, sender_option, sender_value, sender_month, sender_quantity, sender_delta_reference, sender_delta_value, sender_bid, sender_offer }),
  update)

/**
 * @api {delete} /transactions/:id Delete transaction
 * @apiName DeleteTransaction
 * @apiGroup Transaction
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Transaction not found.
 */
router.delete('/:id',
  destroy)

export default router
