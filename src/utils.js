module.exports = {
    USER_TYPE_TRADER        : 'TRADER',
    USER_TYPE_MARKET_MAKER  : 'MARKET_MAKER',
    CURRENCY_SYMBOL         : '$',
    MARKET_MAKERS           : [491337682, 499418762]
}
